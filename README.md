# ember-download-adapter

Adiciona o método download no adapter.

Para usar adicione as dependências:

```javascript
"ember-download-adapter": "git+ssh://git@bitbucket.org/maxicredito/ember-download-adapter#v0.2.1"
```

E usar o adapter:

```javascript
import DownloadMixin from 'ember-download-adapter/mixins/adapters/download';
import DS from 'ember-data';

export default DS.RESTAdapter.extend(DownloadMixin, {});
```

O download do arquivo é feito em duas etapas, uma gera o arquivo e outro faz download do arquivo.
Por exemplo, uma ação:
```javascript
var adapter = this.container.lookup('adapter:application');
adapter.download('/dummy/files', 'POST');
```

Essa requisição deve retornar um json, com uma propriedade "file_path", que será usada para a
próxima requisição do tipo GET para a url '/dummy/files/{file_path}' e fara download do arquivo.