import Ember from 'ember';

export default Ember.Route.extend({
	actions:{
		export: function(){
			var adapter = this.container.lookup('adapter:application');
			adapter.download('/dummy/files', 'POST');
		}
	}
});
