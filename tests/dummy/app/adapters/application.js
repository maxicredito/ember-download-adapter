import DownloadMixin from 'ember-download-adapter/mixins/adapters/download';
import DS from 'ember-data';

export default DS.RESTAdapter.extend(DownloadMixin, {
	headers: {
    "API_KEY": 'abc',
    "ANOTHER_HEADER": "Some header value"
  }
});