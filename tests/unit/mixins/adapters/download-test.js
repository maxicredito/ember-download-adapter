import Ember from 'ember';
import AdaptersDownloadMixin from 'ember-download-adapter/mixins/adapters/download';
import { module, test } from 'qunit';

module('Unit | Mixin | adapters/download');

// Replace this with your real tests.
test('it works', function(assert) {
  var AdaptersDownloadObject = Ember.Object.extend(AdaptersDownloadMixin);
  var subject = AdaptersDownloadObject.create();
  assert.ok(subject);
});
