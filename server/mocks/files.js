var path = require('path');
var mime = require('mime');
var fs = require('fs');

module.exports = function(app) {
  var express = require('express');
  var filesRouter = express.Router();

  filesRouter.post('/', function(req, res){
    var file = 'report.csv';
    console.log('Criando arquivo ' + file);
    res.status(200).send({file_path: file});
  });

  filesRouter.get('/:file', function(req, res){
    var file = req.params.file;
    console.log('Lendo arquivo ' + file);
    var mimetype = mime.lookup(file);

    var filePath = path.join(__dirname, file);
    var stat = fs.statSync(filePath);

    res.setHeader('Content-disposition', 'attachment; filename=' + filePath);
    res.setHeader('Content-type', mimetype);
    res.setHeader('Content-Length', stat.size);

    var readStream = fs.createReadStream(filePath);

    readStream.pipe(res);
  });

  app.use('/dummy/files', filesRouter);
};
