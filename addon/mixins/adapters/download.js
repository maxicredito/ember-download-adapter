import Ember from 'ember';

export default Ember.Mixin.create({
  download: function(url, type, options) {
    var hash = this.ajaxOptions(url, type, options);
    return new Ember.RSVP.Promise(function() {
      hash.xhr = function () {
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'arraybuffer';
        xhr.addEventListener('load', function(){
          var type = xhr.getResponseHeader('Content-Type');
          var file = new Blob([this.response], {type: type});
          var fileURL = URL.createObjectURL(file);

          var link = document.createElement("a");
          link.href = fileURL;
          link.download = type.replace('/', '.').substring(0, type.indexOf(';'));
          Ember.$('body').append(link);
          link.click();
          link.remove();
          Ember.$.unblockUI();
        }, false);
        return xhr;
      };

      Ember.$.ajax(hash);
    }, 'DS: RESTAdapter#download ' + type + ' to ' + url);
  }
});